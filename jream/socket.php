<?php
/**
 * @author      Jesse Boyer <contact@jream.com>
 * @copyright   Copyright (C), 2011-12 Jesse Boyer
 * @license     GNU General Public License 3 (http://www.gnu.org/licenses/)
 *              Refer to the LICENSE file distributed within the package.
 *
 * @link        http://jream.com
 * 
 * DO NOT USE - IN DEVELOPLMENT
 * When complete silence.
 * 
 * Example:
 * // Listen for a Socket (Run in CMD)
 * // $ php your_file.php
 * $socket = new \jream\Socket('127.0.0.1', 99);
 * $socket->listen();
 * 
 * // Send Socket Message (Run anywhere)
 * $socket = new \jream\Socket('127.0.0.1', 99);
 * $socket->send('message');
 */
namespace jream;
class Socket 
{
    // ------------------------------------------------------------------------
    
    private $_address;
    private $_port;
    private $_socket;
    private $_max_clients;
    
    // ------------------------------------------------------------------------
    
    /**
     * __construct - Set the connection options
     * 
     * @param string $address 
     * @param integer $port
     * @param integer $max_clients
     * @param integer $timeout
     */
    public function __construct($address, $port, $max_clients = 10, $timeout = 60)
    {
        $this->_address = $address;
        $this->_port = $port;
        $this->_max_clients = $max_clients;
        $this->_timeout = $timeout;
        $this->_socket = socket_create(AF_INET, SOCK_STREAM, 0);

        if ($this->_socket == false)
        {
            $this->_handleError();
        }
        
    }

    // ------------------------------------------------------------------------
    
    /**
     * run - Runs the socket server
     */
    public function listen()
    {
        $bind = socket_bind($this->_socket, $this->_address , $this->_port);
        $listen = socket_listen($this->_socket, 0);
        
        if ($listen == false)
        {
            $this->_handleError();
        }

        echo "Waiting for connections.. \n";
        
        $client_socks = array();
        $read = array();
        
        /**
         * Trying to implement someones way of handling the clients
         */
        while (true)
        {
            //prepare array of readable client sockets
            $read = array();

            //first socket is the master socket
            $read[0] = $this->_socket;

            //now add the existing client sockets
            for ($i = 0; $i < $this->_max_clients; $i++)
            {
                if(isset($client_socks[$i]) && $client_socks[$i] != null)
                {
                    $read[$i+1] = $client_socks[$i];
                }
            }

            //now call select - blocking call
            if(socket_select($read , $write , $except , null) === false)
            {
                $this->_handleError();
            }

            //if ready contains the master socket, then a new connection has come in
            if (in_array($this->_socket, $read)) 
            {
                for ($i = 0; $i < $this->_max_clients; $i++)
                {
                    if (isset($client_socks[$i]) && $client_socks[$i] == null) 
                    {
                       $client_socks[$i] = socket_accept($this->_socket);

                        //display information about the client who is connected
                        if(socket_getpeername($client_socks[$i], $this->_address, $this->_port))
                        {
                            echo "Client $this->_address : $this->_port is now connected to us. \n";
                        }

                        //Send Welcome message to client
                        $message = "Welcome to server.\n";
                        socket_write($client_socks[$i] , $message);
                        break;
                    }
                }
            }

            //check each client if they send any data
            for ($i = 0; $i < $this->_max_clients; $i++)
            {
                if (isset($client_socks[$i]) && in_array($client_socks[$i] , $read))
                {
                    $input = socket_read($client_socks[$i] , 1024);

                    if ($input == null) 
                    {
                        //zero length string meaning disconnected, remove and close the socket
                        unset($client_socks[$i]);
                        socket_close($client_socks[$i]);
                    }

                    $n = trim($input);

                    $output = "OK ... $input";
                    echo "Sending output to client \n";
                    //send response to client
                    socket_write($client_socks[$i] , $output);
                }
            }
        }
        
    }
    
    // ------------------------------------------------------------------------
    
    /**
     * send - Sends a message to the socket
     * 
     * @param string $message
     */
    public function send($message)
    {
        socket_connect($this->_socket, $this->_address, $this->_port);
        $result = socket_send($this->_socket, $message, strlen($message), 0);
        
        $received = socket_recv($this->_socket, $buf, 2045, MSG_WAITALL);
        echo $buf;
        
        if ($result == false) 
        {
            $this->_handleError();
        }
    }
    
    // ------------------------------------------------------------------------
    
    /**
     * Handles the socket error output
     * 
     * @throws \Exception
     */
    private function _handleError()
    {
        $e_code = socket_last_error();
        $e_msg = socket_strerror($e_code);
            
        die("Socket Error: [$e_code] $e_msg");
    }
    
    // ------------------------------------------------------------------------
 
    /**
     * __destruct - Close the socket when finished
     */
    public function __destruct()
    {
        if ($this->_socket !=  null)
        {
            socket_close($this->_socket);
        }
    }
    
    // ------------------------------------------------------------------------

}